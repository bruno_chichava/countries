package com.brunochichava.countries.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.brunochichava.countries.domain.model.Pais;

@Repository
public interface PaisRepository extends JpaRepository<Pais, Long> {

}
