package com.brunochichava.countries.domain.service;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;

import com.brunochichava.countries.domain.exception.PaisNaoEncontradoException;
import com.brunochichava.countries.domain.model.Pais;
import com.brunochichava.countries.domain.repository.PaisRepository;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class PaisService {

	private static final String MESSAGE_PAIS_NAO_ENCONTRADO = "O pais com o identificador %s nao existe no sistema.";
	
	@Autowired
	private PaisRepository paisRepository;
	
	public Page<Pais> listar(Pageable pageable) {
		pageable = pageableMapping(pageable);
		return paisRepository.findAll(pageable);
	}
	
	public Pais adicionar(Pais pais) {
		return paisRepository.save(pais);
	}
	
	public Pais buscar(Long identificador) {
		return paisRepository.findById(identificador)
				.orElseThrow(() -> new PaisNaoEncontradoException(String.format(MESSAGE_PAIS_NAO_ENCONTRADO, identificador.toString())));
	}
	
	public Pais actualizar(Pais pais, Long identificador) {
		Pais paisAntigo = buscar(identificador);
		BeanUtils.copyProperties(pais, paisAntigo, "identificador", "criado", "modificado");
		
		return paisRepository.save(paisAntigo);
	}
	
	public Pais actualizarParcialmente(Map<String, Object> campos, Long id, HttpServletRequest request) {
		Pais paisActual = buscar(id);
		merge(campos, paisActual, request);
		paisRepository.save(paisActual);
		return paisActual;
	}

	public void remover(Long identificador) {
		Pais pais = buscar(identificador);
		paisRepository.delete(pais);
	}
	
	private void merge(Map<String, Object> camposOrigem, Pais paisDestino, HttpServletRequest request) {
		ServletServerHttpRequest serverHttpRequest = new ServletServerHttpRequest(request);
		
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
			
			Pais paisOrigem = objectMapper.convertValue(camposOrigem, Pais.class);
			
			camposOrigem.forEach((nomePropriedade, valorPropriedade) -> {
				Field field = ReflectionUtils.findField(Pais.class, nomePropriedade);
				field.setAccessible(true);
				
				Object novoValor = ReflectionUtils.getField(field, paisOrigem);
				ReflectionUtils.setField(field, paisDestino, novoValor);
			});
		} catch (IllegalArgumentException e) {
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			throw new HttpMessageNotReadableException(e.getMessage(), rootCause, serverHttpRequest);
		}
	}
	
	private Pageable pageableMapping(Pageable pageable) {
		Map<String, String> mapeamento = Map.of(
					"nome", "nome",
					"capital", "capital",
					"regiao", "regiao",
					"subRegiao", "subRegiao",
					"area", "area"
				);
		
		List<Order> orders = pageable.getSort().stream()
			.filter(order -> mapeamento.containsKey(order.getProperty()))
			.map(order -> new Sort.Order(order.getDirection(), mapeamento.get(order.getProperty())))
			.collect(Collectors.toList());
		
		return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by(orders));
	}
	
}
