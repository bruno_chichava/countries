package com.brunochichava.countries.domain.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Pais {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Include
	private Long identificador;

	private String nome;
	private String capital;
	private String regiao;
	private String subRegiao;
	private String area;

	@CreationTimestamp
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	private LocalDateTime criado;

	@UpdateTimestamp
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	private LocalDateTime modificado;

}
