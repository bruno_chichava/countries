package com.brunochichava.countries.domain.exception;

public class PaisNaoEncontradoException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public PaisNaoEncontradoException(String message) {
		super(message);
	}

}
