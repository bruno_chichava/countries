package com.brunochichava.countries.api.assembler;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Length.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.NotBlank;

@Getter
@Setter
public class PaisInput {

	@NotBlank(message = "O nome do país é obrigatório")
	@List({
		@Length(min = 4, message = "O nome deve conter pelo menos 4 caracteres."),
		@Length(max = 60, message = "O nome deve conter no máximo 60 caracteres.")
	})
	@ApiModelProperty(example = "Moçambique", required = true)
	private String nome;
	
	@NotBlank(message = "A capital do país é obrigatória")
	@List({
		@Length(min = 4, message = "A capital deve conter pelo menos 4 caracteres."),
		@Length(max = 60, message = "A capital deve conter no máximo 30 caracteres.")
	})
	@ApiModelProperty(example = "Maputo", required = true)
	private String capital;
	
	@NotBlank(message = "A região do país é obrigatória")
	@List({
		@Length(min = 4, message = "A região deve conter pelo menos 4 caracteres."),
		@Length(max = 50, message = "A região deve conter no máximo 50 caracteres.")
	})
	@ApiModelProperty(example = "África", required = true)
	private String regiao;
	
	@NotBlank(message = "A subregião do país é obrigatória")
	@List({
		@Length(min = 4, message = "A subregião deve conter pelo menos 3 caracteres."),
		@Length(max = 60, message = "A subregião deve conter no máximo 50 caracteres.")
	})
	@ApiModelProperty(example = "África Austral", required = true)
	private String subRegiao;
	
	@NotBlank(message = "A área do país é obrigatória")
	@List({
		@Length(min = 4, message = "A area deve conter pelo menos 4 caracteres."),
		@Length(max = 60, message = "A area deve conter no máximo 40 caracteres.")
	})
	@ApiModelProperty(example = "Noroeste", required = true)
	private String area;

}
