package com.brunochichava.countries.api.assembler;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel("País")
@Getter
@Setter
public class PaisModel {

	@ApiModelProperty(value = "Identificador do país.", example = "1", position = 1)
	private Long identificador;
	
	@ApiModelProperty(value = "Nome do país.", example = "Moçambique", position = 2)
	private String nome;
	
	@ApiModelProperty(value = "Capital do país.", example = "Maputo", position = 3)
	private String capital;
	
	@ApiModelProperty(value = "Região do país.", example = "África", position = 4)
	private String regiao;
	
	@ApiModelProperty(value = "Subregião do país.", example = "África Austral", position = 5)
	private String subRegiao;
	
	@ApiModelProperty(value = "Área do país.", example = "312679.0", position = 6)
	private String area;
	
}
