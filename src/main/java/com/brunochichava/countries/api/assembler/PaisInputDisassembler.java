package com.brunochichava.countries.api.assembler;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.brunochichava.countries.domain.model.Pais;

@Component
public class PaisInputDisassembler {

	@Autowired
	private ModelMapper mapper;
	
	public Pais toDomainObject(PaisInput paisInput) {
		return mapper.map(paisInput, Pais.class);
	}
	
}
