package com.brunochichava.countries.api.assembler;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.brunochichava.countries.domain.model.Pais;

@Component
public class PaisModelAssembler {

	@Autowired
	private ModelMapper mapper;
	
	public PaisModel toModel(Pais pais) {
		return mapper.map(pais, PaisModel.class);
	}
	
	public List<PaisModel> toCollectionModel(List<Pais> paises) {
		return paises.stream()
				.map(pais -> toModel(pais))
				.collect(Collectors.toList());
	}
	
}
