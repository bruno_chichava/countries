package com.brunochichava.countries.api.exceptionHandler;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.brunochichava.countries.domain.exception.PaisNaoEncontradoException;
import com.fasterxml.jackson.databind.JsonMappingException.Reference;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.PropertyBindingException;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {
	
	@Autowired
	private MessageSource messageSource;
	
	private static final String MSG_ERRO_INTERNO = "Erro na aplicação, por favor reinicie e tente de novo.";

	private static final String TITLE_ERRO_INTERNO = "Erro interno";

	private static final String MSG_PAIS_NAO_ENCONTRADO = "Nao foi possivel encontrar a entidade em causa, reveja e tente de novo.";

	private static final String MSG_PROPRIEDADE_INEXISTENTE = "A propriedade '%s' nao existe, corrija ou remova essa propriedade e tente novamente.";

	private static final String MSG_PROPRIEDADE_INVALIDA = "A propriedade '%s' recebeu o valor '%s' que e de um tipo invalido, corrija e informe um valor compativel com o tipo %s.";

	private static final String MSG_PARAMETRO_INVALIDO = "O parametro de URL '%s' recebeu o valor '%s' que e de um tipo invalido, corrija e informe um valor compativel com o tipo %s.";

	private static final String TITLE_PARAMETRO_INVALIDO = "Parametro invalido";

	private static final String MSG_RECURSO_INEXISTENTE = "O recurso %s que tentou aceder e inexistente.";

	private static final String MSG_RECURSO_NAO_ENCONTRADO = "Recurso nao encontrado";

	private static final String MSG_CORPO_REQUISICAO_INVALIDO = "O corpo da requisição está invalido, verifique a sintaxe e os campos.";

	private static final String TITLE_CAMPOS_INVALIDOS = "Campos Inválidos";
	
	private static final String DETAIL_CAMPOS_INVALIDOS = "Um ou mais campos estão inválidos. Faça o preenchimento correcto e tente novamente.";
	
	private static final String TITLE_PAIS_NAO_ENCONTRADO = "País Não Encontrado.";
	
	private static final String USER_MESSAGE_ERRO_OPERACAO = "Erro ao efectuar a operação, por favor reinicie e tente de novo";
	
	private static final String TITLE_CORPO_REQUISICAO_INVALIDO = "Corpo da requisição inválido";
	
	public static final String MSG_ERRO_GENERICO = "Ocorreu um erro interno inesperado no sistema. Tente novamente e se o problema persistir, entre em contato com o administrador do sistema.";
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> handleUncaught(Exception e, WebRequest request) {
		log.error("Erro no sistema: " + e.getMessage(), e);
		
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;	
		
		String title = TITLE_ERRO_INTERNO;
		String detail = MSG_ERRO_GENERICO;
		String userMessage = MSG_ERRO_INTERNO;
		
		ProblemDetails response = createProblemBuilder(status, title, detail, userMessage).build();
		
		return handleExceptionInternal(e, response, new HttpHeaders(), status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException e, HttpHeaders headers, HttpStatus status, WebRequest request) {
		return handleValidationInternal(e, headers, status, request, e.getBindingResult());
	}
	
	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
		if (body == null) {
			body = createProblemBuilder(status, MSG_ERRO_GENERICO, MSG_ERRO_GENERICO, MSG_ERRO_GENERICO).build();
		} else if (body instanceof String) {
			body = createProblemBuilder(status, (String) body, MSG_ERRO_GENERICO, MSG_ERRO_GENERICO).build();
		}
		
		return super.handleExceptionInternal(ex, body, headers, status, request);
	}
	
	@ExceptionHandler(PaisNaoEncontradoException.class)
	public ResponseEntity<?> handleFielNaoEncontrado(PaisNaoEncontradoException e, WebRequest request) {
		HttpStatus status = HttpStatus.NOT_FOUND;
		
		String title = TITLE_PAIS_NAO_ENCONTRADO;
		String detail = e.getMessage();
		String userMessage = MSG_PAIS_NAO_ENCONTRADO;
		
		ProblemDetails response = createProblemBuilder(status, title, detail, userMessage).build();
		
		return handleExceptionInternal(e, response, new HttpHeaders(), status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleHttpMediaTypeNotAcceptable(HttpMediaTypeNotAcceptableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		return ResponseEntity.status(status).headers(headers).build();
	}
	
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException e,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		Throwable rootCause = ExceptionUtils.getRootCause(e);
		
		if(rootCause instanceof InvalidFormatException) {
			return handleInvalidFormat((InvalidFormatException) rootCause, headers, status, request);
		} else if(rootCause instanceof PropertyBindingException) {
			return handlePropertyBinding((PropertyBindingException) rootCause, headers, status, request);
		}
		
		String title = TITLE_CORPO_REQUISICAO_INVALIDO;
		String detail = MSG_CORPO_REQUISICAO_INVALIDO;
		String userMessage = USER_MESSAGE_ERRO_OPERACAO;
		
		ProblemDetails response = createProblemBuilder(status, title, detail, userMessage).build();
		
		return handleExceptionInternal(e, response, headers, status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException e, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		if(e instanceof MethodArgumentTypeMismatchException) {
			return handleMethodArgumentTypeMismatch((MethodArgumentTypeMismatchException) e, headers, status, request);
		}
		
		return super.handleTypeMismatch(e, headers, status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException e, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		String title = MSG_RECURSO_NAO_ENCONTRADO;
		String detail = String.format(MSG_RECURSO_INEXISTENTE, e.getRequestURL());
		String userMessage = USER_MESSAGE_ERRO_OPERACAO;
		
		ProblemDetails problemDetails = createProblemBuilder(status, title, detail, userMessage).build();
		
		return handleExceptionInternal(e, problemDetails, headers, status, request);
	}
	
	
	@Override
	protected ResponseEntity<Object> handleBindException(BindException e, HttpHeaders headers, HttpStatus status,
			WebRequest request) {
		
		return handleValidationInternal(e, headers, status, request, e.getBindingResult());
	}
	
	private ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException e,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		String title = TITLE_PARAMETRO_INVALIDO;
		String detail = String.format(MSG_PARAMETRO_INVALIDO, 
				e.getName(), e.getValue(), e.getRequiredType().getSimpleName());
		String userMessage = USER_MESSAGE_ERRO_OPERACAO;
		
		ProblemDetails problemDetails = createProblemBuilder(status, title, detail, userMessage).build();
		
		return handleExceptionInternal(e, problemDetails, headers, status, request);
	}

	private ResponseEntity<Object> handleInvalidFormat(InvalidFormatException e,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		String path = joinPath(e.getPath());
		
		String title = TITLE_CORPO_REQUISICAO_INVALIDO;
		String detail = String.format(MSG_PROPRIEDADE_INVALIDA, path, e.getValue(), e.getTargetType().getSimpleName());
		String userMessage = USER_MESSAGE_ERRO_OPERACAO;
		
		ProblemDetails problemDetails = createProblemBuilder(status, title, detail, userMessage).build();
		
		return handleExceptionInternal(e, problemDetails, headers, status, request);
	}
	
	private ResponseEntity<Object> handlePropertyBinding(PropertyBindingException e,
	        HttpHeaders headers, HttpStatus status, WebRequest request) {
		String path = joinPath(e.getPath());
		
		String title = TITLE_CORPO_REQUISICAO_INVALIDO;
		String detail = String.format(MSG_PROPRIEDADE_INEXISTENTE, path);
		String userMessage = USER_MESSAGE_ERRO_OPERACAO;
		
		ProblemDetails problemDetails = createProblemBuilder(status, title, detail, userMessage).build();
		
		return handleExceptionInternal(e, problemDetails, headers, status, request);
	}
	
	private ResponseEntity<Object> handleValidationInternal(Exception ex, HttpHeaders headers,
			HttpStatus status, WebRequest request, BindingResult bindingResult) {
		Throwable t = ExceptionUtils.getRootCause(ex);
		System.out.println(t.getClass());
		
		String title = TITLE_CAMPOS_INVALIDOS;
		String detail = DETAIL_CAMPOS_INVALIDOS;
		String userMessage = DETAIL_CAMPOS_INVALIDOS;
	    
	    List<ProblemDetails.Field> problemObjects = bindingResult.getAllErrors().stream()
	    		.map(objectError -> {
	    			String message = messageSource.getMessage(objectError, LocaleContextHolder.getLocale());
	    			String name = objectError.getObjectName();
	    			
	    			if (objectError instanceof FieldError) {
	    				name = ((FieldError) objectError).getField();
	    			}
	    			
	    			return ProblemDetails.Field.builder()
	    				.name(name)
	    				.userMessage(message)
	    				.build();
	    		})
	    		.collect(Collectors.toList());
	    
	    ProblemDetails problem = createProblemBuilder(status, title, detail, userMessage)
	        .fields(problemObjects)
	        .build();
	    
	    return handleExceptionInternal(ex, problem, headers, status, request);
	}
	

	private String joinPath(List<Reference> references) {
		return references.stream()
				.map(ref -> ref.getFieldName())
				.collect(Collectors.joining("."));
	}

	private ProblemDetails.ProblemDetailsBuilder createProblemBuilder(HttpStatus status, String title, String detail, String userMessage) {
		return ProblemDetails.builder()
				.status(status.value())
				.timestamp(LocalDateTime.now())
				.title(title)
				.detail(detail)
				.userMessage(userMessage);
	}
	
}
