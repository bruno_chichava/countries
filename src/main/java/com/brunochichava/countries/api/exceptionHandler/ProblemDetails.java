package com.brunochichava.countries.api.exceptionHandler;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


@JsonInclude(Include.NON_NULL)
@Builder
@Getter
@Setter
@ApiModel("Problema")
public class ProblemDetails {

	@ApiModelProperty(example = "400", position = 1)
	private Integer status;
	
	@ApiModelProperty(example = "2021-12-13T19:46:38.8568472", position = 2)
	private LocalDateTime timestamp;
	
	@ApiModelProperty(example = "Campos Inválidos", position = 3)
	private String title;
	
	@ApiModelProperty(example = "Um ou mais campos estão inválidos. Faça o preenchimento correcto e tente novamente.", position = 4)
	private String detail;
	
	@ApiModelProperty(example = "Um ou mais campos estão inválidos. Faça o preenchimento correcto e tente novamente.", position = 5)
	private String userMessage;
	
	@ApiModelProperty(value = "Lista de campos que geraram o erro", position = 6)
	private List<Field> fields;
	
	@Getter
	@Builder
	public static class Field {
		@ApiModelProperty(example = "nome")
		private String name;
		
		@ApiModelProperty(example = "O nome do país é obrigatório")
		private String userMessage;
	}
	
}
