package com.brunochichava.countries.api.controller.openapi;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.brunochichava.countries.api.assembler.PaisInput;
import com.brunochichava.countries.api.assembler.PaisModel;
import com.brunochichava.countries.api.exceptionHandler.ProblemDetails;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Países")
public interface PaisControllerOpenApi {

	@ApiOperation("Permite adicionar um pais.")
	PaisModel adicionar(PaisInput paisInput);

	@ApiOperation("Busca todos os paises.")
	Page<PaisModel> listar(Pageable pageable);

	@ApiOperation("Busca os dados de um país pelo identificador.")
	@ApiResponses({
		@ApiResponse(code = 400, message = "Id do país inválido.", response = ProblemDetails.class)
	})
	PaisModel visualizar(@ApiParam(value = "Identificador do país a ser visualizado", example = "1", required = true) Long identificador);

	@ApiOperation("Permite editar os dados de um país passando todos os dados.")
	PaisModel editar(PaisInput paisInput, @ApiParam(value = "Identificador do país a ser editado", example = "1", required = true) Long identificador);

	@ApiOperation("Permite editar um país pelo identificador passando apenas os dados a alterar.")
	@ApiResponses({
		@ApiResponse(code = 404, message = "País não encontrado.", response = ProblemDetails.class)
	})
	PaisModel editarParcial(Map<String, Object> campos, @ApiParam(value = "Identificador do país a ser editado", example = "1", required = true) Long identificador, HttpServletRequest request);

	@ApiOperation("Permite remover um país pelo identificador")
	void remover(@ApiParam(value = "Identificador do país a ser removido", example = "1", required = true) Long identificador);

}