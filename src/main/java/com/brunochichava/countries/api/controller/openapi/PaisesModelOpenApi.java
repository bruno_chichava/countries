package com.brunochichava.countries.api.controller.openapi;

import java.util.List;

import com.brunochichava.countries.domain.model.Pais;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel("PaisesModel")
@Getter
@Setter
public class PaisesModelOpenApi {

	private List<Pais> conteudo;
	
	@ApiModelProperty(value = "Quantidade de registros poa página", example = "10")
	private Long tamanhoPagina;
	
	@ApiModelProperty(value = "Total de registros", example = "50")
	private Long totalElementos;
	
	@ApiModelProperty(value = "Total de páginas", example = "5")
	private Long totalPaginas;
	
	@ApiModelProperty(value = "Número da página (começa em zero)", example = "0")
	private Long paginaActual;
	
}
