package com.brunochichava.countries.api.controller.openapi;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel("Pageable")
@Getter
@Setter
public class PageableModelOpenApi {

	@ApiModelProperty(value = "Número da página (começa em zero)", example = "0")
	private int page;
	
	@ApiModelProperty(value = "Quantidade de elementos por página", example = "5")
	private int size;
	
	@ApiModelProperty(value = "Nome da propriedade para ordenação.", example = "nome,asc")
	private List<String> sort;
	
}
