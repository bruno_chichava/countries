package com.brunochichava.countries.api.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.brunochichava.countries.api.assembler.PaisInput;
import com.brunochichava.countries.api.assembler.PaisInputDisassembler;
import com.brunochichava.countries.api.assembler.PaisModel;
import com.brunochichava.countries.api.assembler.PaisModelAssembler;
import com.brunochichava.countries.api.controller.openapi.PaisControllerOpenApi;
import com.brunochichava.countries.domain.model.Pais;
import com.brunochichava.countries.domain.service.PaisService;

@RestController
@RequestMapping(path =  "/paises", produces = APPLICATION_JSON_VALUE)
public class PaisController implements PaisControllerOpenApi {

	@Autowired
	private PaisService paisService;
	
	@Autowired
	private PaisInputDisassembler paisDisassembler;
	
	@Autowired
	private PaisModelAssembler assembler;
	
	@Override
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public PaisModel adicionar(@RequestBody @Valid PaisInput paisInput) {
		return assembler.toModel(paisService.adicionar(paisDisassembler.toDomainObject(paisInput)));
	}
	
	@Override
	@GetMapping
	public Page<PaisModel> listar(@PageableDefault(size = 10) Pageable pageable) {
		Page<Pais> paisesPage = paisService.listar(pageable);
		List<PaisModel> paisesModel = assembler.toCollectionModel(paisesPage.getContent());
		Page<PaisModel> paisesModelPage = new PageImpl<PaisModel>(paisesModel, pageable, paisesPage.getTotalElements());
		
		return paisesModelPage;
	}
	
	@Override
	@GetMapping("/{identificador}")
	public PaisModel visualizar(@PathVariable Long identificador) {
		return assembler.toModel(paisService.buscar(identificador));
	}
	
	@Override
	@PutMapping("/{identificador}")
	public PaisModel editar(@RequestBody @Valid PaisInput paisInput, @PathVariable Long identificador) {
		return assembler.toModel(paisService.actualizar(paisDisassembler.toDomainObject(paisInput), identificador));
	}
	
	@Override
	@PatchMapping("/{identificador}")
	public PaisModel editarParcial(@RequestBody Map<String, Object> campos, @PathVariable Long identificador, HttpServletRequest request) {
		return assembler.toModel(paisService.actualizarParcialmente(campos, identificador, request));
	}
	
	@Override
	@DeleteMapping("/{identificador}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long identificador) {
		paisService.remover(identificador);
	}
	
}
