# Países

O projecto Países é uma API REST para a manipulação de países desenvolvida em java, o mesmo fornece operações de CRUD para países, ou seja,
permite criar, visualizar, editar e remover países. O projecto foi desenvolvido como requisito para concorrer ao cargo de Backend Engineer na Bi2B.

## Tecnologias Usadas

Linguagem de programação: Java

Gestor de Build: Maven

Framework: Ecossistema Spring

Sistema de Gestão de Bases de dados: MySQL

Servidor de Computação em Núvem: Heroku

### Outras Tecnologias
[Lombok](https://projectlombok.org/) para redução de código boilerplate no projecto

[Flyway](https://flywaydb.org/) para a migração do schema da base de dados.

[Model Mapper](http://modelmapper.org/) para o mapeamento entre tipos de classes e objectos.

[Bean validation](https://beanvalidation.org/) para a validação de entradas na API.

[Apache Commons](https://commons.apache.org/) para utilitários avançados.

[Spring Fox](https://springfox.github.io/springfox/) para a documentação da API com Open API.

## Funcionalidades
### Adicionar País
A API permite adicionar um país com a devida validação dos campos.

### Listar Países
A API permite listar todos os países anteriormente adicionados de forma paginada, permitindo ao cliente da API escolher o tamanho de cada página, a página específica que deseja ver e ainda ordenar por qualquer um dos atributos do país através de query params.

?size - Query param para a quantidade de registros por página;

?page - Query param para a página a ser visualizada (começa de zero);

?sort - Query param para o atributo de ordenação.

Para mais detalhes acerca dos parâmetros de pesquisa ver a documentação da API em [https://bc-countries.herokuapp.com/swagger-ui.html](https://bc-countries.herokuapp.com/swagger-ui.html)

### Visualizar País
A API permite que o cliente visualize os dados de uma único país passando o identificador do país como parâmetro de URL.

### Editar País
A API permite alterar os dados de um país previamente adicionado efectuando a devida validação dos campos passando o identificador do mesmo como parâmetro de URL.

### Editar Parcialmente
Ao passo que a funcionalidade anterior exige que todos os campos do país sejam passados no payload da requisição, esta funcionalidade permite
editar o páis passando no payload apenas os campos que pretendemos alterar efectuando a devida validação.

### Remover País
A API permite ainda remover um páis anteriormente criado passando o seu identificador como parâmetro de URL.

Implementação de exception handler em todos os endpoints da API.

## Como Testar
A API está live através do endereço [https://bc-countries.herokuapp.com/paises](https://bc-countries.herokuapp.com/paises) e a documentação da API pode ser vista através do endereço [https://bc-countries.herokuapp.com/swagger-ui.html](https://bc-countries.herokuapp.com/swagger-ui.html), de salientar que na raiz deste repositório tem a colecção do Postman com todos os endpoints da API prontos para serem testados live.

## Créditos
Desenvolvido por Bruno Estêvão Chichava - bruno.estevao8@gmail.com - [https://www.linkedin.com/in/brunochichava](https://www.linkedin.com/in/brunochichava)
